import requests
import time
from datetime import datetime


def dynamic_dns(domain, username, password):
    ip = requests.get('https://api.ipify.org/')  # Change to https://api6.ipify.org/ if you need IPv6
    user_pass = username + ':' + password
    hostname = domain + '&myip=' + ip.text
    url = 'https://' + user_pass + '@domains.google.com/nic/update?hostname=' + hostname
    result = requests.get(url)
    global response  # makes response global so that it can be used in write_file() and append_file()
    response = result.text


def write_file(domain):  # Use this to create / reset the file output file.
    time_unformatted = time.time()
    timestamp = datetime.fromtimestamp(time_unformatted).strftime('%Y-%m-%d %H:%M:%S')  # Adds timestamp
    f = open('DNNS_output.txt', 'w')
    f.write(timestamp + "\t" + domain + ": " + response)
    f.close()


def append_file(domain):  # This adds to a file after write_file() has been executed.
    time_unformatted = time.time()
    timestamp = datetime.fromtimestamp(time_unformatted).strftime('%Y-%m-%d %H:%M:%S')  # Adds timestamp
    f = open('DNNS_output.txt', 'a')
    f.write("\n\n" + timestamp + "\t" + domain + ": " + response)
    f.close()


while True:  # Makes it so the code never ends. # After the first domain make sure to use append_file!
    # Update the following comment with the credentials from Google Domains
    '''dynamic_dns('subdomain.example.com', 'username', 'password')
    append_file('subdomain.example.com')
    dynamic_dns('subdomain.example.com', 'username', 'password')
    append_file('subdomain.example.com')'''
    time.sleep(3600)  # Waits 3600 seconds ( executes code again )
    open('DNNS_output.txt', 'w').close()
